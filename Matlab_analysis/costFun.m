function f=costFun(x, tracks, track_start, footsteps, stepIndx)
% @(x)costFun(x, tracks, track_start, footsteps, stepIndx)

    footsteps(:,1:2:end)=footsteps(:,1:2:end)+x(1);     % x offset
    footsteps(:,2:2:end)=footsteps(:,2:2:end)+x(2);     % y offset

    f=0;
    N=0;
    % go for all 4 legs
    for k=1:4
        % every step for each leg
        for kk=1:numel(stepIndx{k})-1
            %distance between corresponding track number and all gps points
            %for the stance of the corresponding step
            correspondingFootstep=footsteps(stepIndx{k}(kk)+1 : stepIndx{k}(kk+1), 2*k-1:2*k);
            correspondingTrack = repmat( tracks(track_start(k)+kk-1, 2*k-1:2*k),    size(correspondingFootstep, 1), 1);
%             plot([correspondingFootstep(1,1) correspondingTrack(1,1)], [correspondingFootstep(1,2) correspondingTrack(1,2)], 'o'); hold on; axis equal
            f = f + sum(sqrt( sum(   (correspondingFootstep-correspondingTrack).^2  , 2)  ));
            N=N+size(correspondingFootstep, 1);
        end
    end
    f=f/N;
    
end







