function [footsteps, fval, validatedFootsteps, validatedTracks] = footstepsRegression(footsteps, tracks, stanceIndx)
    
    shouldiplot = 0;
    % regression
    X=reshape(footsteps(:, 1:2:end), numel(footsteps(:, 1:2:end)), 1);
    X=[ones(numel(X),1), X];
    Y=reshape(footsteps(:, 2:2:end), numel(footsteps(:, 2:2:end)), 1);
    B=X\Y;
    x=X(:, 2);
    y=Y;
    
    % get separate steps
    for k=1:4
        [~, stepIndx{k}]=findpeaks(diff(stanceIndx(:,k)));
%         if stepIndx{k}(1)~=1
%             stepIndx{k}=[1; stepIndx{k}];
%         end
%         if stepIndx{k}(end)~=size(stanceIndx(:,k), 1)
%             stepIndx{k}=[stepIndx{k}; size(stanceIndx(:,k), 1)];
%         end
    end
    
    if(shouldiplot)
        figure
        plotTracks(footsteps, tracks, stepIndx);
    end
    
    %  rotation and translation
    rotang=-atan(B(2));
    R=[cos(rotang), -sin(rotang); sin(rotang), cos(rotang)];
    p=R*[x'; y'];
    x2=p(1,:)'; y2=p(2,:)';
    footsteps(:,1:2:end)=reshape(x2, numel(x2)/4, 4);
    footsteps(:,2:2:end)=reshape(y2, numel(y2)/4, 4)-B(1);
    

    
    if(shouldiplot)
        figure
        plotTracks(footsteps, tracks, stepIndx);
    end


    % ALIGN FOR OPTIMIZATION
    % get means of first steps
    for k=1:4
        first{k}=mean(footsteps(stepIndx{k}(1)+1 : stepIndx{k}(2), 2*k-1:2*k));
    end
    % align the tracks to the first front left footstep
    footsteps(:, 1:2:end)=footsteps(:, 1:2:end) + (tracks(4,1) - first{1}(1));
    for k=1:4
        first{k}=mean(footsteps(stepIndx{k}(1)+1 : stepIndx{k}(2), 2*k-1:2*k));
    end
    % find closest tracks to the first steps for the rest
    for k=1:4
        track_start(k)=dsearchn(tracks(:, 2*k-1:2*k), first{k});
    end
        
%     if(shouldiplot)
%         figure
%         plotTracks(footsteps, tracks, stepIndx);
%     end

% put first FL footstep on the closest template
    
    % TEST CONVEXION
%     xoff=-0.2:0.005:0.2;
%     yoff=-0.2:0.005:0.2;
%     for i=1:numel(xoff)
%         i
%         for j=1:numel(yoff)
%             f(i,j)=costFun([xoff(i) yoff(j)], tracks, track_start, footsteps, stepIndx);
%         end
%     end
    x0=[0; 0];
    lb=[-0.2; -0.2];
    ub=[0.2; 0.2];
    options = optimoptions('fmincon','Display','none');
    [xopt, fval] =fmincon(@(x)costFun(x, tracks, track_start, footsteps, stepIndx), ...
        x0,[],[],[],[],lb,ub, [], options);
    
%     disp('no modification: ')
%     costFun([0,0], tracks, track_start, footsteps, stepIndx)
%     disp('optimal: ')
%     fval
    
    footsteps(:,1:2:end)=footsteps(:,1:2:end)+xopt(1);     % x offset
    footsteps(:,2:2:end)=footsteps(:,2:2:end)+xopt(2);     % y offset
 
%     tracks
    for k=1:4
        first{k}(1)=first{k}(1)+xopt(1);
        first{k}(2)=first{k}(2)+xopt(2);
    end
    
    
    % get only validated steps
    
    
    validatedFootsteps=footsteps+NaN;
    validatedTracks=tracks+NaN;
    for k=1:4
        validatedFootsteps(stepIndx{k}(1)+1 : stepIndx{k}(end),2*k-1:2*k)=footsteps(stepIndx{k}(1)+1 : stepIndx{k}(end), 2*k-1:2*k);
        validatedTracks(track_start(k):track_start(k)+numel(stepIndx{k})-1-1,2*k-1:2*k)=tracks(track_start(k):track_start(k)+numel(stepIndx{k})-1-1, 2*k-1:2*k);
    end
    
    
    
    if(shouldiplot)
        figure
        plotTracks(footsteps, tracks, stepIndx);
    end
    
    
    
    
    
    
    %% TRACK MEASUREMENTS
    
    
    
%     clf
%     lw=2;
%     ms=8;
%     linestring='or';
%     hold on
%     axis equal 
%     view(-90, 90)
%     color={'r', 'g', 'b', 'k'}
%     for k=1:4
%         plot(footsteps(:, 2*k-1), footsteps(:, 2*k), ['.' color{k}]), 
%         hold on;
%         plot(tracks(:,2*k-1), tracks(:,2*k), ['o' color{k}], 'linewidth', 1)
%         plot(first{k}(1), first{k}(2), ['-+' color{k}], 'markersize', 20)
%     end
%     
% 
%     hold off;
%     title([num2str(fval) ' score: ' num2str(1/fval)]);
%     xlabel('x')
%     ylabel('y')
%     
% %     figure
% %     [XOFF, YOFF]=meshgrid(xoff, yoff);
% %     surf(XOFF, YOFF, f)
%     
%     pause(3)
end




function plotTracks(footsteps, tracks, stepIndx)
    clf
    lw=2;
    ms=8;
    linestring='or';
    hold on
    axis equal 
    view(-90, 90)
    color={'r', 'g', 'b', 'k'}
    N=size(footsteps,1);
    rangeFootsteps = {stepIndx{1}(1)*0+1:stepIndx{1}(4),stepIndx{2}(1)*0+1:stepIndx{2}(4), stepIndx{3}(1)*1+1:stepIndx{3}(5), stepIndx{4}(1)*0+1:stepIndx{4}(4)};
    rangeTracks={3:6,3:6,3:6,3:6};
%     rangeFootsteps = {stepIndx{1}(1)*0+1:stepIndx{1}(4),stepIndx{2}(1)*0+1:stepIndx{2}(4), stepIndx{3}(1)*1+1:stepIndx{3}(5), stepIndx{4}(1)*0+1:stepIndx{4}(4)};
%     rangeTracks={3:6,3:6,3:6,3:6};
    for k=1:4
        plot(footsteps(rangeFootsteps{k}, 2*k-1), footsteps(rangeFootsteps{k}, 2*k), ['.' color{k}]), 
        hold on;
        plot(tracks(rangeTracks{k},2*k-1), tracks(rangeTracks{k},2*k), ['o' color{k}], 'linewidth', 1)
    end
    

    hold off;
    xlabel('x')
    ylabel('y')

end