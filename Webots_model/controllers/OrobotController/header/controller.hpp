#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include "Eigen/Dense"
#include "Eigen/Geometry"
#include "Eigen/SVD"
#include "joystick.h"
#include "utils.hpp"
#include "misc_math.hpp"
#include <fstream>
#include <pthread.h>
#include <vector>
#include <string.h>
#include <thread>  
#include <mutex> 
/////////////////////////////////
#include <dlib/optimization.h>
#include <dlib/optimization/find_optimal_parameters.h>
#include "qpOASES.hpp"


#define my_pi          3.141592653589793

#define MAX_NUM_MOTORS    40
#define JOYSTICK_DEVNAME "/dev/input/js0"


extern int IS_SIMULATION, USE_JOYSTICK, SPINE_COMPENSATION_WITH_FEEDBACK;
extern int USE_IMU, AUTO_RUN, AUTO_RUN_STATE, LOG_DATA, NUM_MOTORS, SPINE_SIZE;

using namespace Eigen;


enum{WALKING, STANDING, POSING, INITIAL};



class Controller{


  public:
    //================== public variables ===============================================
    Joystick js;
    int state;
    double gamma, freq_walk;
    Matrix<double, Dynamic, 1> fbck_torques, fbck_position, joint_angles;
    Matrix<double, 4, 1> legs_stance;
    vector<Matrix4d> HJs, HJs_g;
    vector<Matrix4d> HJfl, HJfr, HJhl, HJhr, HJfl_g, HJfr_g, HJhl_g, HJhr_g;
    vector<MatrixXd> legJacob;
    Matrix<double, 4, 4> Fgird, Hgird, Fgird0, Hgird0, Fgird_mean, Hgird_mean;
    double hgirdlePhi;
    Vector4d phaseTransitionStanceSwing, phaseTransitionSwingStance, legs_stance_old;
    double IG;


    // needed for RUN
    Matrix<double, Dynamic, 1> spine_kin;
    Matrix<double, 2, 1> girdLoc;
    Matrix<double, 2, 1> spineCPGscaling;
    Matrix<double, 6, 1> trunk_kin;
    Matrix<double, 5, 3> FL_kin, FR_kin, HL_kin, HR_kin;
    Matrix<double, 2, 1> Duty;
    Matrix<double, 4, 1> phShifts;
    Matrix<double, 4, 1> stancePhase, swingPhase, legPhase;
    Matrix<double, 4, 1> legTrackingError;
    Matrix<double, 3, 4> GRF;

    //joystick
    double joy_x1, joy_x2, joy_y1, joy_y2, joy_x3, joy_y3;
    int joy_l1, joy_l2, joy_l3, joy_r1, joy_r2, joy_r3, joy_sel, joy_start, joy_bD, joy_bL, joy_bR, joy_bU;
    double joy_lsr, joy_rsr;
    double joy_lsphi, joy_rsphi;


    Matrix<double, 3, 3> fbck_fgirdRotMat, fgird2ForRotMat, forRotMat;


    double attData[3];
    Vector3d globalPosFL, globalPosFR, globalPosHL, globalPosHR;
    Vector3d gpsPos;
    double FoROrientation;

    // girdle trajectories
    Matrix<double, 6, 2> girdleTraj;
    Matrix<double, 3, 2> girdleVelocity, girdleVelocity_filtered;
    Matrix<double, 3, 2> forVelocity, forVelocity_filtered;
    Matrix<double, 1, 2> girdleAngularVelocity, girdleAngularVelocity_filtered;
    Matrix<double, 1, 2> forAngularVelocity, forAngularVelocity_filtered;
    Matrix<double, 6, 2> forTraj;
    Matrix<double, 3, 200> forTrajHist;
    Matrix<double, 4, 1> q0_trunk_from_spline;
    Matrix<double, 2, 2> girdleRealOrientation;
    Matrix<double, 2, 4> legStanceTraj;
    double walkingDirection;
    double t, dt;
    double walking_forward_velocity, walking_angular_velocity;

    // walking done properly
    Matrix<double, 3, 4> feetReference, feetFeedbackPosition;
    
    Vector2d girdleCpgOutput;


    double or_IGscaling, or_Ltot, or_Lf, or_Lh, or_Wf, or_Wh, or_Df, or_Dh, or_deltaLf, or_deltaLh, or_deltaWf, or_deltaWh,  or_deltaFH, or_ROLLvsYAW;
    Matrix<double, 2, 1>  or_spineCPGscaling;
    double or_heightF, or_heightH, or_lift_height_F, or_lift_height_H, or_stance_end_lift, or_stance_end_lift_exp_const;
    double or_phase_FL, or_deltaphi, or_walk_freq, legs_offset;

    Vector3d FgirdGpsPos;
    Matrix3d FgirdRotMat;
    Vector3d forRPY;
    Matrix<double, 4,4> CinvF, CinvH, MF, MH, or_MF, or_MH;
    Matrix<double, 3, 4> feetGPS;

    //================== public functions ===============================================
    Controller(double time_step); // constructor
    void setTimeStep(double time_step);
    bool runStep();
    void readJoystick();
    bool updateState();
    void getAngles(double table[MAX_NUM_MOTORS]);
    void getTorques(double table[MAX_NUM_MOTORS]);
    void forwardKinematics();
    std::vector<Matrix<double, 4, 4>>  legKinematics(Vector4d q, int leg);
    MatrixXd Jacob(Vector4d q, int leg);
    void updateRobotState(double *d_posture, double *d_torque);
    void getAttitude(double *rotmat);
    void GRFfeedback(double grf[12]);

    

    
    
    // playing with torque
    bool DirectTorqueSetup();



    // static walking
    Vector3d getSwingTrajectory(Vector3d initPoint, Vector3d middlePoint, Vector3d finalPoint, double phase, int leg);
    bool moveSwingLeg(int leg);
    void moveBody(Vector3d bodyVelocity, double headingAngularVelocity);
    void moveGirdle(Vector3d girdleVelocity, double girdleAngularVelocity, int girdleNum);
    void walkingStateMachine();
    Vector3d trunkForwardKinematics(Vector3d fgird_pos, MatrixXd q_trunk);
    void getWalkingFrequency();

    //std::vector<Matrix<double,3,4>> predictedFootsteps;
    std::vector<Matrix<double,3,4>> supportPolys;
    void initOrobot();
    void getGPS(double *gps, double *gps_feet1, double *gps_feet2, double *gps_feet3, double *gps_feet4);



  private:
    //================== private variables ===============================================
    vector<qpOASES::QProblemB> ikinQpSolver; 
    int ikin_maxIter;
    double ikin_tol, ikin_max_speed;
    double T_trans, T_trans0;
    Vector3d ikin_constr_penalty;
    // joystick objects
    js_event event;
    double maxSpeed;



    Matrix<double, 3, 4> midStance;
    Matrix<double, 1, 4> ellipse_a, ellipse_b, swing_height, swing_width;
    double trAnglesF0[4], trAnglesH0[4], bezierParamF0[2], bezierParamH0[2];
    Matrix<double, 3, 4> stanceStart, stanceEstEnd;

    // constraints on joint angles
    Matrix<double, 2, 4> constrFL, constrHL, constrFR, constrHR; 
    double constrS;


    Matrix<double, 3, 1> pFL, pFR, pHL, pHR;
    double angSignsCorrAnimal[MAX_NUM_MOTORS], angShiftsCorrAnimal[MAX_NUM_MOTORS];
    double angSignsCorrIkin2Webots[MAX_NUM_MOTORS], angShiftsCorrIkin2Webots[MAX_NUM_MOTORS];
    double angSignsCorrIkin2Robot[MAX_NUM_MOTORS], angShiftsCorrIkin2Robot[MAX_NUM_MOTORS];
    double angSignsCorrWebots2Robot[MAX_NUM_MOTORS], angShiftsCorrWebots2Robot[MAX_NUM_MOTORS];

    Matrix<double, Dynamic, 1> angles, torques;


    // initial conditions
    Vector4d q0FL, q0FR, q0HL, q0HR, qFL, qFR, qHL, qHR;
    Matrix<double, Dynamic, 1> init_angles;


    // auxiliary variables
    Matrix<double, Dynamic, 1> qs;
    
    double max_dist;
    Vector2d lamF, lamH;

    // dynamics
    double Tf1; // trajectory filtering

    // filter parameters
    Transform<double,3,Affine> HFL, HHL, HFR, HHR;



    
    //================== private functions ===============================================

    Vector4d iKinNullIDLS(int leg, Vector3d pref, Vector4d qref, Vector4d q0, Vector2d lam, Matrix4d M, 
                            double max_dist, int maxIter, double tol, MatrixXd constr, Vector3d constr_penalty);
    Vector4d iKinQpOases(int leg, Vector3d pref, Vector4d qref, Vector4d q0, Vector2d lam, Matrix4d M, 
                            double max_dist, int maxIter, double tol, MatrixXd constr, double max_speed, double* err);
    void joystickManipulation();
    bool getParameters();
    void walkingTrajectories();
    MatrixXd transformation_Ikin_Webots(MatrixXd joint_angles, int direction, int shifts);
    MatrixXd transformation_Ikin_Robot(MatrixXd joint_angles, int direction, int shifts);
    void inverseKinematicsController();
    void legPhaseDynamics(double dt);
    void girdleTrajectories(double v, double w);
    void legSwingTrajectory();
    void legStanceTrajectory();
    void getLegJacobians();
    std::vector<Matrix<double,3,4>> predictTrajectories(int N, double time_step, MatrixXd *predLegPhase);
    void girdleOscillations();
    void FixedGirdleOscillations();
    void getSwingStanceEndPoints();
    


    
};


#endif
